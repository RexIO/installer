#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:

package RexIO::Base;

use Rex -base;
use Rex::Commands::User;
use Rex::Lang::Perl::Perlbrew;
use Digest::Bcrypt;
use Rex::CMDB;

task "setup_perlbrew", sub {
  my $params = shift;

  perlbrew -install => ( 'cpanm', 'patchperl', $params->{version} );
  perlbrew -use => $params->{version};

  run "cpanm Carton";
  if($? != 0) {
    Rex::Logger::info("Error installing Carton.");
    die("Error installing Carton.");
  }
};

task "setup_base", sub {
  pkg [
    qw/
      at
      bridge-utils
      ntpdate
      expat-devel
      curl
      gcc
      make
      m4
      autoconf
      automake
      bison
      flex
      git
      /
    ],
    ensure => "latest";

  run "ntpdate pool.ntp.org";
};

our $dns_key;

task "setup_bind", sub {
  my $conf = get cmdb;

  pkg "bind", ensure => "latest";

  if ( is_file("/etc/named/rexio-key.conf") ) {
    my $content = cat "/etc/named/rexio-key.conf";
    ($dns_key) = ( $content =~ m/secret "([^"]+)";/ms );
  }
  else {
    my $lines = join( "\n", run("ddns-confgen -a hmac-md5") );
    ($dns_key) = ( $lines =~ m/secret "([^"]+)";/ms );
  }

  file "/etc/named/rexio-key.conf",
    content => template( '@key.tpl', key => $dns_key, conf => $conf );

  file "/etc/named/rexio-acl.conf",
    content => template( '@acl.tpl', conf => $conf );

  file "/etc/named/rexio-controls.conf",
    content => template( '@controls.tpl', conf => $conf );

  for my $zone ( @{ $conf->{dns}->{zones} } ) {
    file "/etc/named/$zone->{name}.zone.conf",
      content => template( '@zone.tpl', conf => $conf, %{$zone} );

    file "/var/named/$zone->{name}.zone",
      content      => template( '@zone-file.tpl', conf => $conf, %{$zone} ),
      owner        => "named",
      group        => "named",
      no_overwrite => TRUE;

    append_if_no_such_line "/etc/named.conf",
      "include \"/etc/named/$zone->{name}.zone.conf\";";
  }

  file "/var/named",
    ensure => "directory",
    owner  => "named",
    group  => "named";

  append_if_no_such_line "/etc/named.conf",
    'include "/etc/named/rexio-key.conf";';
  append_if_no_such_line "/etc/named.conf",
    'include "/etc/named/rexio-acl.conf";';
  append_if_no_such_line "/etc/named.conf",
    'include "/etc/named/rexio-controls.conf";';

  service named => "restart";
  service named => ensure => "started";
};

task "setup_dhcp", sub {
  my $conf = get cmdb;

  pkg "dhcp", ensure => "latest";

  my $network = $conf->{dhcp}->{subnet}->{network};
  my $netmask = $conf->{dhcp}->{subnet}->{netmask};

  append_if_no_such_line "/etc/dhcp/dhcpd.conf", "omapi-port 7911;";

  append_if_no_such_line "/etc/dhcp/dhcpd.conf",
    regexp => qr/subnet $network netmask $netmask/,
    line   => template( '@dhcp-subnet.tpl', conf => $conf );

  service dhcpd => ensure => "started";
};

task "setup_tftpd", sub {
  pkg [qw/tftp-server wget/], ensure => "latest";

  run "download-ipxe",
    command =>
    "wget -O /var/lib/tftpboot/undionly.ipxe http://boot.ipxe.org/undionly.kpxe",
    creates => "/var/lib/tftpboot/undionly.ipxe";

  file "/etc/xinetd.d/tftp",
    source => "files/tftp.xinetd.conf",
    owner  => "root",
    group  => "root",
    mode   => 644;

  service "xinetd", ensure => "running";
};

task "setup_httpd", sub {
  pkg "httpd", ensure => "latest";

  service httpd => ensure => "started";
};

task "setup_mysqld", sub {
  pkg [
    qw/
      mysql-server
      mysql-devel
      /
    ],
    ensure => "latest";

  service mysqld => ensure => "started";
};

task "setup_mysql_db", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  my @lines = grep { m/^rexio$/ } run "echo 'SHOW SCHEMAS' | mysql -uroot";

  if ( !@lines ) {
    file "/tmp/rexio.sql",
      content => "CREATE SCHEMA `$conf->{database}->{schema}`;
                     GRANT ALL PRIVILEGES ON `$conf->{database}->{schema}`.* to `$conf->{database}->{user}`@`$conf->{database}->{user_host}` IDENTIFIED BY '$conf->{database}->{password}';";

    # todo: handle different db server and password auth
    run
      "cat /tmp/rexio.sql | mysql -u$conf->{database}->{admin_user} && rm /tmp/rexio.sql";
    if ( $? != 0 ) {
      die("Error setting up datebase schema.");
    }

    run
      "cat $install_root/software/rex-io-server/db/*.sql | mysql -u$conf->{database}->{user} -p$conf->{database}->{password} $conf->{database}->{schema}";
    if ( $? != 0 ) {
      die("Error creating initial database layout.");
    }

    my $content = cat "/etc/rex/io/server.conf";
    my $config =
      eval 'package Rex::IO::Server::Config::Loader;' . "no warnings; $content";

    die "Couldn't load configuration file: $@" if ( !$config && $@ );
    die "Config file invalid. Did not return HASH reference."
      if ( ref($config) ne "HASH" );

    my $salt = $config->{auth}->{salt};
    my $cost = $config->{auth}->{cost};

    for my $user ( @{ $conf->{rexio}->{user} } ) {
      my $bcrypt = Digest::Bcrypt->new;
      $bcrypt->salt($salt);
      $bcrypt->cost($cost);
      $bcrypt->add( $user->{password} );

      my $pw = $bcrypt->hexdigest;

      file "/tmp/user.sql",
        content =>
        "INSERT INTO users (name, password) VALUES('$user->{name}', '$pw')\n";

      run
        "cat /tmp/user.sql | mysql -u$conf->{database}->{user} -p$conf->{database}->{password} $conf->{database}->{schema} && rm /tmp/user.sql";
      if ( $? != 0 ) {
        die("Error creating initial user.");
      }
    }
  }
};

task "setup_redis", sub {
  run "add-epel",
    command =>
    "rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm",
    creates => "/etc/yum.repos.d/epel.repo";

  if ( $? != 0 ) {
    die("Error setting up EPEL repository.");
  }

  pkg "redis", ensure => "latest";

  service "redis", ensure => "started";
};

task "setup_user", sub {
  my $conf = get cmdb;
  create_group "rex.io";

  create_user "rex.io",
    home   => $conf->{rexio}->{install_root},
    groups => ["rex.io"];
};

1;

__DATA__

@key.tpl
key "rexio" {
	algorithm hmac-md5;
	secret "<%= $key %>";
};
@end

@acl.tpl
acl trusted-servers {
   // the ip of the server where rex.io is running. use 127.0.0.1 if it is the same as where the dns is running
   <%= $conf->{dns}->{trusted_server} %>;
};
@end

@controls.tpl
controls {
   // the ip of the server where rex.io is running. use 127.0.0.1 if it is the same as where the dns is running
   inet 127.0.0.1 port 953 allow { any; }
   keys { "rexio"; };
};
@end

@zone.tpl
zone "<%= $name %>" IN {
   type master;
   file "<%= $name %>.zone";
   allow-transfer { trusted-servers; };
   allow-update { key "rexio"; };
};
@end

@zone-file.tpl
$TTL	3600
$ORIGIN <%= $name %>.
@  1D  IN  SOA ns1.<%= $name %>. hostmaster.<%= $name %>. (
			      2014020401 ; serial
			      3H         ; refresh
			      15         ; retry
			      1w         ; expire
			      3h         ; minimum
			     )
       IN  NS     ns1.<%= $name %>.
ns1    IN  A      127.0.0.1
@end

@dhcp-subnet.tpl
subnet <%= $conf->{dhcp}->{subnet}->{network} %> netmask <%= $conf->{dhcp}->{subnet}->{netmask} %> {
   range <%= $conf->{dhcp}->{subnet}->{range} %>;
   option routers <%= $conf->{dhcp}->{subnet}->{router} %>;

   if exists user-class and option user-class = "iPXE" {
      filename "http://<%= $conf->{rexio}->{server}->{ip} %>:<%= $conf->{rexio}->{server}->{port} %>/deploy/boot?deploy=true";
   } else {
      filename "undionly.ipxe";
   }
}
@end
