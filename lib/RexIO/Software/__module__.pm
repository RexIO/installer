#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:

package RexIO::Software;

use Rex -base;
use Rex::CMDB;
use Rex::Commands::Iptables;
use Rex::Lang::Perl::Carton;

my $VERSION = "0.4";

task "prepare_common", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  file $install_root,
    ensure => "directory",
    owner  => "root",
    group  => "root";

  file "$install_root/bin",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  file "/etc/rex/io",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  file "/var/run/rex-io",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  file "$install_root/bin/daemon",
    content => template( 'templates/daemon.tpl', conf => $conf ),
    owner   => "root",
    group   => "root",
    mode    => 755;

  if ( $conf->{firewall} ) {
    open_port [ 80, 443, 3000, 5000 ], proto => "tcp";
    open_port [69], proto => "udp";
  }
};

task "setup_server", sub {
  my $params       = shift;
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  prepare_common();

  _checkout("git://github.com/RexIO/rex-io-server.git");
  carton "install", cwd => "$install_root/software/rex-io-server";

  if ( $? != 0 ) {
    die('Error installing dependencies for Rex.IO Server');
  }

  file "/etc/rex/io/server.conf",
    content => template( '@rexio-server.conf', %{$params}, conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/sysconfig/rex-io-server",
    content => template( '@sysconfig-rex-io-server.tpl', conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/init.d/rex-io-server",
    content => template(
    'templates/init/rex-io.init.sh',
    name           => 'rex-io-server',
    conf_file_name => 'server',
    conf           => $conf
    ),
    owner => "root",
    group => "root",
    mode  => 755;

  file "$install_root/software/rex-io-server/log",
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755;
};

task "setup_dhcp_agent", sub {
  my $params       = shift;
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  prepare_common();

  _checkout("git://github.com/RexIO/rex-io-dhcp-agent.git");
  carton "install", cwd => "$install_root/software/rex-io-dhcp-agent";

  if ( $? != 0 ) {
    die('Error installing dependencies for Rex.IO DHCP Agent');
  }

  file "/etc/rex/io/dhcp-agent.conf",
    content => template( '@rexio-dhcp-agent.conf', %{$params}, conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/sysconfig/rex-io-dhcp-agent",
    content => template( '@sysconfig-rex-io-dhcp-agent.tpl', conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/init.d/rex-io-dhcp-agent",
    content => template(
    'templates/init/rex-io.init.sh',
    name           => 'rex-io-dhcp-agent',
    conf_file_name => 'dhcp-agent',
    conf           => $conf
    ),
    owner => "root",
    group => "root",
    mode  => 755;

  file "$install_root/software/rex-io-dhcp-agent/log",
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755;

};

task "setup_webui", sub {
  my $params       = shift;
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  prepare_common();

  _checkout("git://github.com/RexIO/rex-io-webui.git");
  carton "install", cwd => "$install_root/software/rex-io-webui";

  if ( $? != 0 ) {
    die('Error installing dependencies for Rex.IO WebUI');
  }

  file "/etc/rex/io/webui.conf",
    content => template( '@rexio-webui.conf', %{$params}, conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/sysconfig/rex-io-webui",
    content => template( '@sysconfig-rex-io-webui.tpl', conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/init.d/rex-io-webui",
    content => template(
    'templates/init/rex-io.init.sh',
    name           => 'rex-io-webui',
    conf_file_name => 'webui',
    conf           => $conf
    ),
    owner => "root",
    group => "root",
    mode  => 755;

  file "$install_root/software/rex-io-webui/log",
    ensure => "directory",
    owner  => "root",
    group  => "root",
    mode   => 755;

};

task "setup_client", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  _checkout("git://github.com/RexIO/rex-io-client.git");

  run "cpanm --notest --installdeps .",
    cwd => "$install_root/software/rex-io-client";

  if ( $? != 0 ) {
    die('Error installing dependencies for Rex.IO Client Library.');
  }

  run "perl Makefile.PL && make && make install",
    cwd => "$install_root/software/rex-io-client";

  if ( $? != 0 ) {
    die("Error installing Rex.IO Client Library.");
  }
};

task "setup_agent", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  prepare_common();

  _checkout("git://github.com/RexIO/rex-io-service-os-daemon.git");

  run "cpanm --notest FusionInventory::Agent";

  carton "install", cwd => "$install_root/software/rex-io-service-os-daemon";

  if ( $? != 0 ) {
    die('Error installing dependencies for Rex.IO Service OS Daemon.');
  }

  file "/etc/rex/io/service-daemon.conf",
    content => template( '@rexio-service-daemon.conf', conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/rex/io/server",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  file "/etc/rex/io/server/service-daemon.conf.tpl",
    content => template( '@rexio-service-daemon.conf', conf => $conf ),
    owner   => "root",
    group   => "root";

  file "/etc/rex/io/inventory",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  cp "$install_root/software/rex-io-service-os-daemon/doc/bootdevice.inventory",
    "/etc/rex/io/inventory/bootdevice";
  cp "$install_root/software/rex-io-service-os-daemon/doc/bridge.inventory",
    "/etc/rex/io/inventory/bridge";
  cp "$install_root/software/rex-io-service-os-daemon/doc/sysinfo.inventory",
    "/etc/rex/io/inventory/sysinfo";
  cp "$install_root/software/rex-io-service-os-daemon/doc/os.inventory",
    "/etc/rex/io/inventory/os";

  file [
    "/etc/rex/io/inventory/bootdevice", "/etc/rex/io/inventory/bridge",
    "/etc/rex/io/inventory/sysinfo",    "/etc/rex/io/inventory/os"
    ],
    owner => "root",
    group => "root",
    mode  => 755;

  file "/etc/init.d/rex-io-service-os-daemon",
    content =>
    template( "templates/init/rex-io-service-os-daemon.init", conf => $conf ),
    owner => "root",
    group => "root",
    mode  => 755;
};

task "start_services", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  run "cpanm --notest Net::Server";
  if ( $? != 0 ) {
    die("Error installing Net::Server");
  }

  service [ "rex-io-dhcp-agent", "rex-io-server", "rex-io-webui" ],
    ensure => "running";
};

task "register_server", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  run "fusioninventory-agent --stdout >/tmp/fi.xml 2>/dev/null";
  service "rex-io-service-os-daemon" => ensure => "running";
};

task "download_service_os", sub {
  run "download-service-os",
    command =>
    "wget -O /tmp/service-os.tar.gz http://rex.io/downloads/service-os.tar.gz",
    creates => "/var/www/html/boot/rexio/rexio.squashfs";

  run "extract-service-os",
    command => "tar xzf /tmp/service-os.tar.gz",
    cwd     => "/var/www/html",
    creates => "/var/www/html/boot/rexio/rexio.squashfs";
};

task "setup_boot_targets", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  my $sql =
    "INSERT INTO `os_template` VALUES (2,'Inventory','http://$conf->{rexio}->{server}->{ip}/boot/rexio/vmlinuz','http://$conf->{rexio}->{server}->{ip}/boot/rexio/initrd.img','REXIO_SERVER=$conf->{rexio}->{server}->{ip}:$conf->{rexio}->{server}->{port} boot=live fetch=http://$conf->{rexio}->{server}->{ip}/boot/rexio/rexio.squashfs noeject ssh=test lang=de vga=0 noudev fast','','');";
  file "/tmp/sql.tmp", content => $sql;

  run "add-service-os",
    command =>
    "cat /tmp/sql.tmp | mysql -u$conf->{database}->{user} -p$conf->{database}->{password} $conf->{database}->{schema}",
    unless =>
    "mysql -u$conf->{database}->{user} -p$conf->{database}->{password} $conf->{database}->{schema} -e 'SELECT id FROM os_template WHERE id=2' | grep -q 2";

  unlink "/tmp/sql.tmp";
};

################################################################################
# private functions
################################################################################

sub _checkout {
  my ($url)        = @_;
  my ($comp)       = ( $url =~ m/\/([^\/]+)\.git$/ );
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  file "$install_root/software",
    ensure => "directory",
    owner  => "root",
    group  => "root";

  if ( is_dir("$install_root/software/$comp/.git") ) {
    run "git checkout .", cwd => "$install_root/software/$comp";

    run "git fetch remote; git checkout $VERSION; git pull origin $VERSION",
      cwd => "$install_root/software/$comp";
  }
  else {
    run "git clone $url",        cwd => "$install_root/software";
    run "git checkout $VERSION", cwd => "$install_root/software/$comp";
  }
}

1;

__DATA__

@rexio-server.conf
{
  # dns configuration
  # currently only BIND is supported
  dns  => {
    server => "<%= $conf->{dns}->{server} %>",            # ip of your DNS server
    key   => "<%= $dns_key %>",  # the key you've just generated with ddns-confgen
    key_name => "rexio",             # the keyname
    tlds => [                    # the zone that rex should manage
    <% for my $dom (@{ $conf->{dns}->{zones} }) { %>
      "<%= $dom->{name} %>",
    <% } %>
    ],
  },

  dhcp => {
    server => "<%= $conf->{rexio}->{'dhcp-agent'}->{ip} %>:<%= $conf->{rexio}->{'dhcp-agent'}->{port} %>",        # the ip and port on which the rex-io-dhcp-agent is listening on
  },

  # plugins that should be loaded
  plugins => [
    "Host",
    "Hardware",
    #"ServerGroup",
    <% for my $plg (@{ $conf->{rexio}->{server}->{plugins} }) { %>
    "<%= $plg %>",
    <% } %>
  ],

  # database configuration
  # currently only mysql is supported
  database => {
    host    => "<%= $conf->{database}->{server} %>",
    schema  => "<%= $conf->{database}->{schema} %>",
    username => "<%= $conf->{database}->{user} %>",
    password => "<%= $conf->{database}->{password} %>",
  },

  redis => {
    deploy => {
      server => "localhost",
      port  => 6379,
      queue  => "rex_io_deploy",  # please don't change the queue names, yet
    },
    jobs => {
      server => "localhost",
      port  => 6379,
      queue  => "rex_io_deploy",  # please don't change the queue names, yet
    },
  },

  # configure hypnotoad webserver
  hypnotoad => {
    listen => ["http://<%= $conf->{rexio}->{server}->{ip} %>:$conf->{rexio}->{server}->{port}?"     # use no ssl yet, listen on port 5000 on every device
              #. "cert=/home/jan/temp/rexssl/ca/certs/pitahaya.local.crt"
              #. "&key=/home/jan/temp/rexssl/ca/private/pitahaya.local.key"
              #. "&ca=/home/jan/temp/rexssl/ca/certs/ca.crt"
           ],
  },

  # see https://metacpan.org/module/Digest::Bcrypt for more information
  auth => {
    salt => '9dn46DkL2AYdkuFa', # must be 16 bytes long. use your own!
    cost => 1,               # between 1 and 31, (1=fast and less secure, 31=slow)
  },

  # session settings
  session => {
    key => 'Rex.IO.Server',
  },

  agent => {
    keep_alive_timeout => 90,
    keep_alive_flap => 3,
  },

}
@end

@rexio-webui.conf
{
  server => {
    url    => "http://<%= $conf->{rexio}->{server}->{ip} %>:<%= $conf->{rexio}->{server}->{port} %>",
    user    => "<%= $conf->{rexio}->{user}->[0]->{name} %>",     # use the user you've just created for Rex.IO Server
    password => "<%= $conf->{rexio}->{user}->[0]->{password} %>",
  },

  session => {
    key => "Rex.IO.WebUI",
  },

  plugins => [
    "Server",
    <% for my $plg (@{ $conf->{rexio}->{webui}->{plugins} }) { %>
    "<%= $plg %>",
    <% } %>
    "User",
    # you don't want to use these plugins, yet
    #"Monitoring",
    #"Log",
  ],

};
@end

@sysconfig-rex-io-server.tpl
# rex-io-server parameters file
REXIO_PATH=<%= $conf->{rexio}->{install_root} %>/software/rex-io-server
REXIO_COMMAND=${REXIO_PATH}/bin/rex_ioserver
REXIO_BIND_TO="http://<%= $conf->{rexio}->{server}->{ip} %>:<%= $conf->{rexio}->{server}->{port} %>"
@end

@sysconfig-rex-io-webui.tpl
# rex-io-webui parameters file
REXIO_PATH=<%= $conf->{rexio}->{install_root} %>/software/rex-io-webui
REXIO_COMMAND=${REXIO_PATH}/bin/rex_ioweb_ui
REXIO_BIND_TO="http://<%= $conf->{rexio}->{webui}->{ip} %>:<%= $conf->{rexio}->{webui}->{port} %>"
@end

@sysconfig-rex-io-dhcp-agent.tpl
# rex-io-server parameters file
REXIO_PATH=<%= $conf->{rexio}->{install_root} %>/software/rex-io-dhcp-agent
REXIO_COMMAND=${REXIO_PATH}/bin/rex_iodhcpagent
REXIO_BIND_TO="http://<%= $conf->{rexio}->{'dhcp-agent'}->{ip} %>:<%= $conf->{rexio}->{'dhcp-agent'}->{port} %>"
@end



@rexio-dhcp-agent.conf
{
  dhcp_server => "<%= $conf->{rexio}->{'dhcp-agent'}->{'dhcp-server'} %>",
  lease_file  => "/var/lib/dhcpd/dhcpd.leases",
}
@end

@rexio-service-daemon.conf
{
  user  => "root",
  group  => "root",
  pid_file => "/var/run/rex-io-service-daemon.pid",

  rexio => {
    server => "<%= $conf->{rexio}->{server}->{ip} %>",
    port  => <%= $conf->{rexio}->{server}->{port} %>,
  },

  log  => {
    file  => "/var/log/rex-io-service-daemon.log",
    level => "debug",
  },

  ping => {
    interval => 15,
  },

  plugins => [
    "Ping",
  ],

};
@end
