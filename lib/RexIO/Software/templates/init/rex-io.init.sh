#!/bin/sh
#
# <%= $name %>        init file for starting up the <%= $name %> daemon
#
# chkconfig:   - 20 80
# description: Starts and stops the <%= $name %> daemon.

# Source function library.
. /etc/rc.d/init.d/functions

name="<%= $name %>"
exec="<%= $conf->{rexio}->{install_root} %>/bin/daemon"
pidfile="/var/run/rex-io/<%= $name %>.pid"
lockfile=/var/lock/subsys/<%= $name %>

REXIO_CONFIG=/etc/rex/io/<%= $conf_file_name %>.conf
REXIO_SYSCONFIG=/etc/sysconfig/<%= $name %>


if [ ! -e "${REXIO_SYSCONFIG}" ]; then
    echo "Can't find ${REXIO_SYSCONFIG}."
    exit 6
fi

. ${REXIO_SYSCONFIG}

export PERL5LIB=${REXIO_PATH}/lib:${REXIO_PATH}/local/lib/perl5
export PERLLIB=${PERL5LIB}

. <%= $conf->{rexio}->{install_root} %>/etc/bashrc
perlbrew use <%= $conf->{perl}->{version} %>

start() {
    [ -f $REXIO_CONFIG ] || exit 6
    [ -x $exec ] || exit 5
    echo -n $"Starting $name: "
    $exec --user ${REXIO_USER-root} --group ${REXIO_GROUP-root} --command ${REXIO_COMMAND} --bind ${REXIO_BIND_TO} --pid_file $pidfile
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $name: "
    if [ ! -e "$pidfile" ]; then
        echo "No pidfile found ($pidfile)"
        exit 1
    fi
    killproc -p $pidfile
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    sleep 1    # give time to free listen port
    start
}

reload() {
    false
}

rh_status() {
    status -p $pidfile $name
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart}"
        exit 2
esac
exit $?
