#!/bin/sh
#
# rex-io-service-os-daemon        init file for starting up the rex-io-service-os-daemon daemon
#
# chkconfig:   - 20 80
# description: Starts and stops the rex-io-service-os-daemon daemon.

# Source function library.
. /etc/rc.d/init.d/functions

name="rex-io-service-os-daemon"
exec="<%= $conf->{rexio}->{install_root} %>/software/rex-io-service-os-daemon/bin/rex-io-service-os-daemon"
pidfile="/var/run/rex-io-service-daemon.pid"
lockfile=/var/lock/subsys/${name}

REXIO_CONFIG=/etc/rex/io/service-daemon.conf

export PERL5LIB=<%= $conf->{rexio}->{install_root} %>/software/rex-io-service-os-daemon/lib:<%= $conf->{rexio}->{install_root} %>/software/rex-io-service-os-daemon/local/lib/perl5
export PERLLIB=${PERL5LIB}

start() {
    [ -f $REXIO_CONFIG ] || exit 6
    [ -x $exec ] || exit 5
    echo -n $"Starting $name: "
    fusioninventory-agent --stdout 2>/dev/null >/tmp/fi.xml
    $exec start
    retval=$?
    echo
    [ $retval -eq 0 ] && touch $lockfile
    return $retval
}

stop() {
    echo -n $"Stopping $name: "
    if [ ! -e "$pidfile" ]; then
        echo "No pidfile found ($pidfile)"
        exit 1
    fi
    killproc -p $pidfile
    retval=$?
    echo
    [ $retval -eq 0 ] && rm -f $lockfile
    return $retval
}

restart() {
    stop
    sleep 1    # give time to free listen port
    start
}

reload() {
    false
}

rh_status() {
    status -p $pidfile $name
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}


case "$1" in
    start)
        rh_status_q && exit 0
        $1
        ;;
    stop)
        rh_status_q || exit 0
        $1
        ;;
    restart)
        $1
        ;;
    reload)
        rh_status_q || exit 7
        $1
        ;;
    force-reload)
        force_reload
        ;;
    status)
        rh_status
        ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart|try-restart}"
        exit 2
esac
exit $?
