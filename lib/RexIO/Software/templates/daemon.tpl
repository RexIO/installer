#!<%= $conf->{rexio}->{install_root} %>/perls/<%= $conf->{perl}->{version} %>/bin/perl
use strict;
use warnings;

use Net::Server::Daemonize 'daemonize';
use Getopt::Long;
use File::Basename;

my ( $user, $group, $pid_file, $command, $bind_to );

GetOptions(
  "user=s"     => \$user,
  "group=s"    => \$group,
  "pid_file=s" => \$pid_file,
  "bind_to=s"  => \$bind_to,
  "command=s"  => \$command
);

if ( !$user ) {
  print "No user given.\n";
  exit 1;
}

if ( !$group ) {
  print "No group given.\n";
  exit 1;
}

if ( !$pid_file ) {
  print "No pid_file given.\n";
  exit 1;
}

if ( !$command ) {
  print "No command given.\n";
  exit 1;
}

daemonize( $user, $group, $pid_file );

$ENV{PATH}     = dirname($^X) . ":$ENV{PATH}";
$ENV{PERL5LIB} = dirname($command) . "/../local/lib/perl5";

exec $command, 'daemon', '-l', $bind_to;
