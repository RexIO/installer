#
# AUTHOR: jan gehring <jan.gehring@gmail.com>
# REQUIRES:
# LICENSE: Apache License 2.0
#
# Simple Module to install Perlbrew on your Server.
#
#
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
#

package Rex::Lang::Perl::Carton;

use strict;
use warnings;

use Rex -base;

require Exporter;
use base qw(Exporter);
use vars qw(@EXPORT);

@EXPORT = qw(carton);

sub carton {
  my ( $command, %option ) = @_;

  my $cwd = $option{cwd};
  if ( !$cwd ) {
    Rex::Logger::info( "No cwd defined.", "error" );
    die("No cwd defined.");
  }

  run "carton-install-$cwd",
    command => "carton $command",
    cwd     => $cwd;
}

1;
