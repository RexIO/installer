#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=3 sw=3 tw=0:
# vim: set ft=perl:
# vim: set expandtab:

use Rex -feature => '0.44';
use Rex::Lang::Perl::Perlbrew;
use Rex::CMDB;
use List::Util;

set cmdb => {
  type => "YAML",
  path => "conf",
};

include qw/
  RexIO::Base
  RexIO::Software
  /;

task "setup", sub {
  my $conf         = get cmdb;
  my $install_root = $conf->{rexio}->{install_root};

  update_package_db;

  RexIO::Base::setup_base();

  set perlbrew => root => $install_root;

  if ( !is_dir("$install_root/perls/$conf->{perl}->{version}") ) {
    RexIO::Base::setup_perlbrew(
      {
        version => $conf->{perl}->{version},
      }
    );
  }

  perlbrew -use => $conf->{perl}->{version};

  RexIO::Base::setup_bind()
    if ( grep { $_ eq "dns" } @{ $conf->{infrastructure} } );
  RexIO::Base::setup_dhcp()
    if ( grep { $_ eq "dhcp" } @{ $conf->{infrastructure} } );
  RexIO::Base::setup_tftpd()
    if ( grep { $_ eq "tftp" } @{ $conf->{infrastructure} } );
  RexIO::Base::setup_httpd()
    if ( grep { $_ eq "httpd" } @{ $conf->{infrastructure} } );
  RexIO::Base::setup_mysqld()
    if ( grep { $_ eq "database" } @{ $conf->{infrastructure} } );
  RexIO::Base::setup_redis()
    if ( grep { $_ eq "redis" } @{ $conf->{infrastructure} } );

  RexIO::Software::setup_server(
    {
      dns_key => $RexIO::Base::dns_key,
    }
  ) if ( grep { $_ eq "server" } @{ $conf->{components} } );
  RexIO::Software::setup_dhcp_agent()
    if ( grep { $_ eq "dhcp-agent" } @{ $conf->{components} } );
  RexIO::Software::setup_webui()
    if ( grep { $_ eq "webui" } @{ $conf->{components} } );
  RexIO::Software::setup_client()
    if ( grep { $_ eq "webui" } @{ $conf->{components} } );

  RexIO::Base::setup_mysql_db()
    if ( grep { $_ eq "server" } @{ $conf->{components} } );

  RexIO::Software::setup_agent()
    if ( grep { $_ eq "agent" } @{ $conf->{components} } );
  RexIO::Software::start_services();
  RexIO::Software::register_server()
    if ( grep { $_ eq "agent" } @{ $conf->{components} } );

  RexIO::Software::download_service_os();
  RexIO::Software::setup_boot_targets();
};
